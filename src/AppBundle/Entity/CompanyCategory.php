<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * CompanyCategory
 *
 * @ORM\Table(name="company_category", uniqueConstraints={@ORM\UniqueConstraint(name="title", columns={"title"})})
 * @ORM\Entity
 */
class CompanyCategory
{
    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    private $title;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Company", mappedBy="idCategory")
     */
    private $idCompany;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->idCompany = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Set title
     *
     * @param string $title
     *
     * @return CompanyCategory
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add idCompany
     *
     * @param \AppBundle\Entity\Company $idCompany
     *
     * @return CompanyCategory
     */
    public function addIdCompany(\AppBundle\Entity\Company $idCompany)
    {
        $this->idCompany[] = $idCompany;

        return $this;
    }

    /**
     * Remove idCompany
     *
     * @param \AppBundle\Entity\Company $idCompany
     */
    public function removeIdCompany(\AppBundle\Entity\Company $idCompany)
    {
        $this->idCompany->removeElement($idCompany);
    }

    /**
     * Get idCompany
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIdCompany()
    {
        return $this->idCompany;
    }
}
