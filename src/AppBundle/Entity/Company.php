<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Company
 *
 * @ORM\Table(name="company", indexes={@ORM\Index(name="id_city", columns={"id_city"}), @ORM\Index(name="id_state", columns={"id_state"})})
 * @ORM\Entity
 */
class Company
{
    /**
     * @Groups({"elastica"})
     * 
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=false)
     */
    private $description;

    /**
     * @Groups({"elastica"})
     * 
     * @var string
     *
     * @ORM\Column(name="phone_number", type="string", length=20, nullable=false)
     */
    private $phoneNumber;

    /**
     * @Groups({"elastica"})
     * 
     * @var string
     *
     * @ORM\Column(name="zip_code", type="string", length=20, nullable=false)
     */
    private $zipCode;

    /**
     * @Groups({"elastica"})
     * 
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255, nullable=false)
     */
    private $address;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\IbgeCity
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\IbgeCity")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_city", referencedColumnName="id")
     * })
     */
    private $idCity;

    /**
     * @var \AppBundle\Entity\IbgeState
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\IbgeState")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_state", referencedColumnName="id")
     * })
     */
    private $idState;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\CompanyCategory", inversedBy="idCompany")
     * @ORM\JoinTable(name="company_categories",
     *   joinColumns={
     *     @ORM\JoinColumn(name="id_company", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="id_category", referencedColumnName="id")
     *   }
     * )
     */
    private $idCategory;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->idCategory = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Set title
     *
     * @param string $title
     *
     * @return Company
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Company
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set phoneNumber
     *
     * @param string $phoneNumber
     *
     * @return Company
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * Get phoneNumber
     *
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * Set zipCode
     *
     * @param string $zipCode
     *
     * @return Company
     */
    public function setZipCode($zipCode)
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    /**
     * Get zipCode
     *
     * @return string
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Company
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idCity
     *
     * @param \AppBundle\Entity\IbgeCity $idCity
     *
     * @return Company
     */
    public function setIdCity(\AppBundle\Entity\IbgeCity $idCity = null)
    {
        $this->idCity = $idCity;

        return $this;
    }

    /**
     * Get idCity
     *
     * @return \AppBundle\Entity\IbgeCity
     */
    public function getIdCity()
    {
        return $this->idCity;
    }
    
    /**
     * Set idState
     *
     * @param \AppBundle\Entity\IbgeState $idState
     *
     * @return Company
     */
    public function setIdState(\AppBundle\Entity\IbgeState $idState = null)
    {
        $this->idState = $idState;

        return $this;
    }

    /**
     * Get idState
     *
     * @return \AppBundle\Entity\IbgeState
     */
    public function getIdState()
    {
        return $this->idState;
    }

    /**
     * Add idCategory
     *
     * @param \AppBundle\Entity\CompanyCategory $idCategory
     *
     * @return Company
     */
    public function addIdCategory(\AppBundle\Entity\CompanyCategory $idCategory)
    {
        $this->idCategory[] = $idCategory;

        return $this;
    }

    /**
     * Remove idCategory
     *
     * @param \AppBundle\Entity\CompanyCategory $idCategory
     */
    public function removeIdCategory(\AppBundle\Entity\CompanyCategory $idCategory)
    {
        $this->idCategory->removeElement($idCategory);
    }

    /**
     * Get idCategory
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIdCategory()
    {
        return $this->idCategory;
    }
    
    /**
     * Get city name
     * @return String City name
     */
    public function getCityName() {
        return $this->idCity->getName();
    }
    
    /**
     * Get state name
     * @return String State name
     */
    public function getStateName() {
        return $this->idState->getName();
    }
    
    /**
     * Get all categories named joined with ','
     * @return String Categories names
     */
    public function getCategoriesNames() {
        $names = [];
        foreach($this->idCategory as $cat) {
            $names[] = $cat->getTitle();
        }
        return implode(', ', $names);
    }
}
