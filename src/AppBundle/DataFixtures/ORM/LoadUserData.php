<?php
namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\User;

/**
* NOT USED.
* Created for testing user load from database
* Required classes removed. Keeped for future reference.
*/
class LoadUserData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $userAdmin = new User();
        $userAdmin->setUsername('ale');
        $userAdmin->setEmail('lu.ale.monteiro@gmail.com');
        $userAdmin->setPassword('testing');

        $manager->persist($userAdmin);
        $manager->flush();
    }
}
?>