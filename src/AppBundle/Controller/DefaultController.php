<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig' );
    }
    
    /**
     * @Route("/search", name="search")
     */
    public function actionSearch(Request $request)
    {
        $query = $request->query->get('q');
        
        $results = $this->_searchBySQL($query);
        
        // replace this example code with whatever you need
        return $this->render('default/search-result.html.twig', [
            'results' => $results,
            'term' => $query
        ]);
    }
    
    /**
     * Search company with Elastic Search.
     * NOT FUNCTIONAL - Elastic search disabled in config due to problems with CRUD
     * 
     * @private
     * @param String $query Term to query
     */
    protected function _searchByElastic($query) {
        $finder = $this->container->get('fos_elastica.finder.search.company');
        $results = $finder->find($query);
        return $results;
    }
    
    /**
     * Search company with custom SQL Query
     * @private
     * @param String $query Term to query
     * @return Array Companies found
     */
    protected function _searchBySQL($query) {
        
        $em = $this->getDoctrine()->getManager();
        
        $sql = "SELECT comp.id as `id` FROM company as comp  
                INNER JOIN ibge_state as state ON ( comp.id_state = state.id)
                INNER JOIN ibge_city as city ON ( comp.id_city = city.id)
            WHERE 
                (comp.title LIKE '%$query%') OR
                (comp.address LIKE '%$query%') OR
                (comp.zip_code LIKE '%$query%') OR
                (city.`name` LIKE '%$query%') OR
                (comp.id IN ( SELECT id_company FROM company_categories cc 
                                INNER JOIN company_category as cat ON (cc.id_category = cat.id )
                            WHERE cat.title LIKE '%$query%')
                )";
        
        $qb = $em->createQueryBuilder();
        
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $resp = $stmt->fetchAll();
        
        if (count($resp) > 0 ) {
            $ids = [];
            foreach($resp as $row) {
                $ids[] = $row['id'];
            }
            $str_ids = implode(",", $ids);
            $result = $em->getRepository("AppBundle:Company")->createQueryBuilder('c')
                        ->where("c.id IN ($str_ids)")
                        ->getQuery()
                        ->getResult();
        }
        else {
            $result = [];
        }
        return $result;
    }
    
    /**
     * @Route("/business/view/{id}", name="view business")
     */
    public function actionViewBusiness(Request $request, EntityManagerInterface $em, $id)
    {
        $company = $em->getRepository('AppBundle:Company')->find($id);
        
        return $this->render('default/company-view.html.twig', [
            'company' => $company
        ]);
    }
}
