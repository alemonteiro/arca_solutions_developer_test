<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use AppBundle\Entity\Company;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class AdminController extends Controller
{
    /**
     * Admin home / List all registered business
     * 
     * @Route("/admin", name="admin")
     */
    public function indexAction(Request $request, EntityManagerInterface $em, AuthenticationUtils $authUtils)
    {
		$companys = $em->getRepository('AppBundle:Company')->findAll();
        
        return $this->render('admin/index.html.twig', [
            'companys' => $companys
        ]);
    }
    
    /**
     * @Route("/admin/business/edit/{id}", name="admin/business/edit")
     */
    public function updateCompanyAction(Request $request, EntityManagerInterface $em, $id) {
        $company = $em->getRepository('AppBundle:Company')->find($id);
        
        $form = $this->_createForm($company, true);
        
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
             if ($form->get('save')->isClicked()) {
                 $em->persist($company);
                 $em->flush();

                $request->getSession()
                    ->getFlashBag()
                    ->add('success', 'Bussiness <b>'.$company->getTitle().'</b> updated successfully!');
                return $this->redirectToRoute('admin');
             } elseif ($form->get('delete')->isClicked()) {
                $em->remove($company);
                $em->flush();  

                $this->addFlash('success', "Business <b>{$company->getTitle()}</b> has been deleted successfully!");
                return $this->redirectToRoute('admin');
             }
        }
        return $this->render('admin/company-form.html.twig', array(
            'title' => 'Edit Business',
            'form' => $form->createView(),
        ));
    }
    
    /**
     * @Route("/admin/business/add", name="admin/business/add")
     */
    public function addCompanyAction(Request $request, EntityManagerInterface $em)
    {
        $company = new Company();
        
        $form = $this->_createForm($company);
        
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $company = $form->getData();

             $em->persist($company);
             $em->flush();

            $request->getSession()
                ->getFlashBag()
                ->add('success', 'Bussiness <b>'.$company->getTitle().'</b> added with success.');
            ;
            
            return $this->redirectToRoute('admin');
        }
        
        return $this->render('admin/company-form.html.twig', array(
            'title' => 'Add Business',
            'form' => $form->createView(),
        ));
    }
    
    /**
     * Return cities options to load with ajax
     * 
     * @Route("/admin/ajax/cities/{idState}", name="ajax_cities")
     */
    public function ajaxCities(Request $request, $idState)
    {
        $em = $this->getDoctrine()->getManager();

        $cities = $em->getRepository('AppBundle:IbgeCity')->findBy([
            'idState' => $idState
        ]);

        if(null === $cities )
        {
            return new Response('');
        }

        $options = '';

        foreach($cities as $city)
        {
            $options .= '<option class="city_'.$city->getId().'" value="'.$city->getId().'">'.$city->getName().'</option>';
        }

        return new Response($options);
    }
    
    
    /**
     * Create form layout
     * @private
     * @param  AppBundle\Entity\Company $company Company object
     * @param  Boolean [$editing=false] If the form is for a new input or to edit an existing company
     * @return Symfony\Component\Form\Forms\Form  Generated form
     */
    protected function _createForm($company, $editing=false) {
        
        $form = $this->createFormBuilder($company)
            ->add('title', TextType::class)
            ->add('phoneNumber', TextType::class, [
                'label' => 'Phone number',
                'attr' => ['data-mask' => '(00) 0000-00009']
            ])
            ->add('address', TextType::class)
            ->add('zipCode', TextType::class, [
                'label' => 'Zip code',
                'attr' => ['data-mask' => '00000-000']
            ])
            ->add('idState', EntityType::class, [
                'label' => 'State',
                'class' => 'AppBundle:IbgeState',
                'choice_label' => function($state, $key, $index) {
                    return $state->getName();
                },
                'choice_attr' => function($state, $key, $index) {
                    return ['class' => 'state_'.$state->getId()];
                },
                'placeholder' => 'Choose the state'
            ])
            ->add('idCity', EntityType::class, [
                'label' => 'City',
                'class' => 'AppBundle:IbgeCity',
                //'choices' => []
                'choice_label' => function($city, $key, $index) {
                    return $city->getName();
                },
                'choice_attr' => function($city, $key, $index) {
                    return ['class' => 'city_'.$city->getId()];
                },
                //'placeholder' => 'Choose the state'
            ])
            ->add('description', TextareaType::class)
            ->add('idCategory', EntityType::class, [
                'label' => 'Category',
                'class' => 'AppBundle:CompanyCategory',
                'choice_label' => function($category, $key, $index) {
                    return $category->getTitle();
                },
                'choice_attr' => function($category, $key, $index) {
                    return ['class' => 'category_'.$category->getId()];
                },
                'multiple' => true,
                'expanded' => true
            ])
            ->add('save', SubmitType::class, [
                'label' => $editing ? 'Update Business' : 'Add Business',
                'attr' => ['class' => 'btn-primary save']
            ]);
        
        if ( $editing ) {
            $form->add('delete', SubmitType::class, [
                'label' => 'Delete this business',
                'attr' => ['class' => 'btn-link delete']
            ]);
        }
        
        return $form->getForm();
    }
    
}
