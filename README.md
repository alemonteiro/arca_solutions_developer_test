arca_developer_test
=========

Developer test for Arca Solutions, using: 

* PHP 7.0
* MySQL 5.6
* Symfony 3.3
* Droctrine 2.5
* Twig 2.4

##  Prerequisites

Besides those listed above, also is needed: 

* Composer
* Bower

## Setup

#### Database

* Create a database and import **database.sql**
* Configure **app/config/parameters.yml** with your database credentials

#### Install Dependencies

Run on prompt
```
composer install
```

```
bowser install
```

## Admin login

** /admin **

* Login: admin
* Password: testing

Login and password are loaded from memory and configured in **app/config/security.xml**

## Remarks

* No pagination implemented
* Elastic search not functional and disabled
* Not many errors treated, including global exceptions (like unexpected routes)